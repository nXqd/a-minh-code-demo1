const R = require('ramda')
const fr = require('./domain/friend_request')
const log = require('winston')
const shared = require('./shared')

/**
 * Updates user's radius
 * @param db
 * @param data
 * @returns {Promise.<void>}
 */
exports.activateFriendLinkAPI = async (db, data) => {
  const {uid, friendId} = data

  // get user id from friend id
  const fid = await shared.findFriendId(db, friendId)
  if (R.isNil(fid)) {
    log.error(`User with friendId: ${fid} is not found.`)
    return
  }

  if (fid.available === false) {
    log.error(`User with friendId: is not available.`)
    return
  }

  // uid: person who clicked on the link
  // friend / friendId: person to whom the link belongs
  const requesterUid = fid.uid
  const requesteeUid = uid
  fr.createFriendRequestWithFriendId(db, requesterUid, requesteeUid, friendId)
}
