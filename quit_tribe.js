const R = require('ramda')
const itwilio = require('./infra/twilio')
const log = require('winston')
const shared = require('./shared')
const tribed = require('./domain/tribe')
const userd = require('./domain/user')

/**
 * Quit tribe message payload
 * @param {String} quitterName
 * @returns {{notification: {title: string, body: string, sound: string}}}
 */
exports.quitTribePayload = (quitterName) => {
  return {
    notification: {
      title: `A member has quit the tribe.`,
      body: `${quitterName} has quit the tribe.`,
      sound: 'default'
    }
  }
}

/**
 * User quits tribe
 * - Update database
 * - Send notification to all other tribe members
 * @param admin
 * @param db
 * @param {String} tid - Tribe's id
 * @param {String} uid
 * @param {String} userDisplayName
 */
const quitTribe = (admin, db, tid, uid, userDisplayName) => {
  userd.quitTribe(db, tid, uid)

  const messagePayload = exports.quitTribePayload(userDisplayName)
  userd.sendMessageToTribeMembers(admin, db, uid, messagePayload)
}

/**
 * user tribe name from user displayName
 * @param displayName
 */
const userTribeName = (displayName) => `${displayName}'s tribe!`

/**
 * Remove user from tribe
 * @param {Object} twilioChatService
 * @param {Object} db
 * @param {String} uid
 * @param {Object} user
 * @returns {Promise.<void>}
 */
const removeUserFromTribeChannel = async (twilioChatService, db, uid, user) => {
  const tribeId = shared.userTribeId(user)
  const tribe = await shared.findTribe(db, tribeId)
  if (R.isNil(tribe)) {
    shared.errorMissingTribe(tribeId)
    return
  }

  return await itwilio.removeUserFromChannel(twilioChatService, tribe.twilioChannelSid, user.twilioUserSID)
}

/**
 * Check if member is alone user of a tribe
 * @param tribe
 * @return {Boolean}
 */
const is1MemberTribe = (tribe) =>  {
  const members = tribe.members

  // Filter member with true value and count
  let count = 0
  R.map(
    ([k, v]) => {
      if (v === true) { count += 1 }
    },
    R.toPairs(members)
  )
  return (count <= 1)
}

/**
 * Update user's tribeId
 * @param {Object} db
 * @param {String} uid
 * @param {String} tribeId
 * @returns {boolean}
 */
const updateUserTribeId = (db, uid, tribeId) => {
  try {
    db.ref(shared.usersPath)
      .child(uid)
      .child('private')
      .update({
        tribeId: tribeId
      })
    return true
  } catch (err) {
    log.error(`Error while updating user's radius: ${err}`)
    return false
  }
}

/**
 * Create new tribe for tribe's owner
 * @param {Object} db
 * @param {Object} user
 * @param {String} uid
 * @returns {Null | boolean}
 */
const createNewOwnerTribe = async (db, user, uid) => {
  const tribeName = userTribeName(shared.userDisplayName(user))
  const [newTribeId, newTribeChannel] = await tribed.createTribe(db, twilioChatService, tribeName)
  if (R.isNil(newTribeId)) {
    log.error(`Tribe cannot be created.`)
    return null
  }

  // update current user's tribeId
  updateUserTribeId(db, uid, newTribeId)

  tribed.addTribeMember(db, newTribeId, uid)
  userd.addUserToChannel(db, newTribeChannel, user, uid)
}

/**
 * quits tribe
 * @param admin
 * @param db
 * @param {Object} twilioChatService
 * @param {Object} data
 * @returns {Promise.<void>}
 */
exports.quitTribeAPI = async (admin, db, twilioChatService, data) => {
  log.info(data)

  const {uid} = data

  const user = await shared.findUser(db, uid)
  if (R.isNil(user)) {
    shared.errorMissingUser(uid)
    return
  }

  const tribeId = shared.userTribeId(user)
  const tribe = await shared.findTribe(db, tribeId)
  if (R.isNil(tribe)) {
    shared.errorMissingTribe(tribeId)
    return
  }

  if (is1MemberTribe(tribe)) {
    log.error(`This tribe only has 1 member.`)
    // return
  }

  let ok
  quitTribe(admin, db, tribeId, uid, shared.userDisplayName(user))

  const userAccessToken = shared.userTwilioAccessToken(user)
  if (R.isNil(userAccessToken)) {
    ok = await itwilio.generateAccessTokenForUser(user, "APPLE")
    if (R.isNil(ok)) {
      log.info('Cannot generate access token for user.')
      return
    }
  } else {
    ok = await removeUserFromTribeChannel(twilioChatService, db, uid, user)
    if (R.isNil(ok)) {
      log.info(`Cannot remove user from tribe channel.`)
      // return // we can still continue with this.
    }
  }

  const tribeName = userTribeName(shared.userDisplayName(user))
  const [newTribeId, newTribeChannel] = await tribed.createTribe(db, twilioChatService, tribeName)
  if (R.isNil(newTribeId)) {
    log.error(`Tribe cannot be created.`)
    return
  }

  // update current user's tribeId
  updateUserTribeId(db, uid, newTribeId)

  tribed.addTribeMember(db, newTribeId, uid)
  userd.addUserToChannel(db, newTribeChannel, user, uid)
}
