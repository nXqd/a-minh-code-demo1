const R = require('ramda')
const log = require('winston')
const shared = require('./shared')

/**
 * Create login payload
 * @param userName
 * @returns {*}
 */
const loginPayload = (userName) => {
  if (R.isNil(userName)) {
    log.warn("userName is nil")
    return null
  }

  return {
    notification: {
      title: 'User logged in.',
      body: `${userName} + " has logged in."`,
      sound: 'default'
    }
  }
}

/**
 * Sends remote notification to other tribe members
 * @param admin
 * @param db
 * @param uid
 * @param members
 * @param payload
 * @returns {Promise.<void>}
 */
const sendNotificationToTribeMembers = async (admin, db, uid, members, payload) => {
  if (R.isNil(members) || R.isEmpty(members) || members.length === 1) {
    log.warn(`Tribe is empty or has only one member.`)
    return
  }

  R.map(
    async (id) => {
      if (id === uid) return
      shared.sendMessage(admin, db, id, payload)
    },
    Object.keys(members)
  )
}

/**
 * Trigger some functions while user logins
 * @param admin
 * @param db
 * @param data
 * @returns {Promise.<void>}
 */
exports.userLoginAPI = async (admin, db, data) => {
  const {uid} = data

  const user = await shared.findUser(db, uid)
  if (R.isNil(user)) {
    shared.errorMissingUser(uid)
    return
  }

  const {members} = await shared.findTribe(db, shared.userTribeId(user))
  if (R.isNil(members) || R.isEmpty(members)) {
    log.warn(`Tribe members is empty.`)
    return
  }

  const _loginPayload = loginPayload(shared.userDisplayName(user))
  sendNotificationToTribeMembers(admin, db, uid, members, _loginPayload)
}
