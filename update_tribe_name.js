const shared = require('./shared')
const R = require('ramda')
const log = require('winston')

/**
 * updates tribe's name
 * @param db
 * @param {String} tribeId
 * @param {String} tribeName
 * @returns {Promise.<void>}
 */
const updateTribeName = async (db, tribeId, tribeName) => {
  try {
    db.ref(shared.tribesPath).child(tribeId).update({
      tribeName: tribeName
    })
  } catch (err) {
    log.error(`Error while updating tribe's name: ${err}`)
  }
}

/**
 * updates tribe's name
 * @param db
 * @param data
 * @returns {Promise.<void>}
 */
exports.updateTribeNameAPI = async (db, data) => {
  const {uid, tribeName} = data

  const user = await shared.findUser(db, uid)
  if (R.isNil(user)) {
    shared.errorMissingUser(uid)
    return
  }

  // DZUNG:
  //  - Read tribe's channel SID.
  //  - Update tribe's channel name.

  updateTribeName(db, shared.userTribeId(user), tribeName)
}
