const R = require('ramda')
const shared = require('./shared')
const shortid = require('shortid')
const log = require('winston')

/**
 * Updates user's friendId
 * @param db
 * @param {String} uid
 * @param {String} fid
 */
const updateFriendId = (db, uid, fid) => {
 db.ref(shared.usersPath)
    .child(uid)
    .child('public')
    .child('friendId')
    .set(fid)

  db.ref(shared.friendIdsPath)
    .child(fid)
    .set({
      available: true,
      uid: uid
    })
}

/**
 * Check if friendId is available
 * @param {Object} db
 * @param {String} friendId
 */
const isFriendIdAvaiable = async (db, friendId) => {
  const fid = await shared.findFriendId(db, friendId)
  return R.isNil(fid)
}

/**
 * Revokes friend id
 * @param {Object} db
 * @param {String} uid
 * @param {String} fid
 */
const revokeFriendId = (db, uid, fid) => {
  if (R.isNil(fid)) {
    return
  }

  db.ref(shared.friendIdsPath)
    .child(fid)
    .child('available')
    .set(false)
}

/**
 * Refreshes friend id
 * @param db
 * @param uid
 * @param oldFid
 * @returns {Promise.<void>}
 */
const refreshFriendId = async (db, uid, oldFid) => {
  revokeFriendId(db, uid, oldFid)

  let isFriendIdAvailable = false
  let shortId

  while (!isFriendIdAvailable) {
    shortId = shortid.generate()
    isFriendIdAvailable = await isFriendIdAvaiable(db, shortId)
  }

  updateFriendId(db, uid, shortId)
}

/**
 * Updates user's radius
 * @param db
 * @param data
 * @returns {Promise.<void>}
 */
exports.refreshFriendIdAPI = async (db, data) => {
  const {uid} = data

  const user = await shared.findUser(db, uid)
  if (R.isNil(user)) {
    shared.errorMissingUser(uid)
    return
  }

  const friendId = shared.userFriendId(user)
  refreshFriendId(db, uid, friendId)
}
