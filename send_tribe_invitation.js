const R = require('ramda')
const log = require('winston')
const shared = require('./shared')

/**
 * creates tribe invitation
 * @param db
 * @param {String} inviterUid
 * @param {String} inviteeUid
 * @param {String} tribeId
 */
const createTribeInvitation = (db, inviterUid, inviteeUid, tribeId) => {
  if (R.isNil(inviteeUid) || R.isNil(inviterUid)) {
    log.error('[createTribeInvitation] Inviter uid or Invitee uid is missing.')
    log.error(`inviterUid: ${inviterUid}, inviteeUid: ${inviteeUid}`)
    return
  }

  const invitation = db.ref('/data/tribeInvitations').push()
  invitation.set({
    'inviterUid': inviterUid,
    'inviteeUid': inviteeUid,
    'tribeId': tribeId
  })

  return invitation.key
}

/**
 * returns tribe id of a user
 * @param db
 * @param uid
 * @returns {String}
 */
const tribeIdOf = async (db, uid) => {
  const user = await shared.findUser(db, uid)
  return shared.userTribeId(user)
}

/**
 * @param admin
 * @param db
 * @param {Object} data
 */
exports.sendTribeInvitationsAPI = async (admin, db, data) => {
  const {uid, invitees} = data

  if (R.isEmpty(invitees)) {
    log.error('[sendTribeInvitationsAPI] invitees is empty.')
    return
  }

  const tribeId = await tribeIdOf(db, uid)

  R.mapObjIndexed((val, invideeUid, obj) => {
    if (!val) return

    const iid = createTribeInvitation(db, uid, invideeUid, tribeId)
    shared.addTribePendingInvitations(db, tribeId, iid)
    shared.addUserInvitation(db, invideeUid, iid)
  }, invitees)
}
