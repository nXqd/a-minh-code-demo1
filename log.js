const log = require('winston'),
  WinstonCloudWatch = require('winston-cloudwatch')

if (process.env.NODE_ENV === 'production') {
  log.add(WinstonCloudWatch, {
    logGroupName: '/app/nodejs',
    logStreamName: 'Application logging',
    awsRegion: 'us-east-1',
    jsonMessage: true
  })
}

log.info("Init log config completed.")

exports.log = log

