const R = require('ramda')
const log = require('winston')

exports.friendIdsPath = '/data/friendIds'
exports.friendRequestsPath = '/data/friendRequests'
exports.friendShipsPath = '/data/friendships'
exports.tribeInvitationsPath = '/data/tribeInvitations'
exports.tribesPath = '/data/tribes'
exports.usersPath = '/data/users'

/*
  Check valid number
 */
exports.isValidNumber = R.both(R.is(Number), R.complement(R.equals(NaN)))

/**
 * @param admin
 * @param db
 * @param {String} uid
 * @param {Object} payloadObject
 */
exports.sendMessage = async (admin, db, uid, payloadObject) => {
  let user = await exports.findUser(db, uid)
  const {firebaseRegistrationToken} = user.private

  if (R.isNil(firebaseRegistrationToken)) {
    log.error("User doesn't have registration token")
    return
  }
  admin.messaging().sendToDevice(firebaseRegistrationToken, payloadObject)
}

/**
 * @param db
 * @param {String} path
 * @param {String} id
 * @returns {Null|Object}
 */
const find = async (db, path, id) => {
  if (!id) return
  let snapshot
  try {
    snapshot = await db.ref(path).child(id).once('value')
  } catch (err) {
    log.error('Error in finding.')
    return
  }

  if (R.isNil(snapshot)) {
    return
  }

  return snapshot.val()
}

/**
 * @param db
 * @param {String} id
 * @returns {Null|Object}
 */
exports.findUser = async (db, id) => {
  return find(db, exports.usersPath, id)
}

/**
 * @param db
 * @param {String} id
 * @returns {Null|Object}
 */
exports.findInvitation = async (db, id) => {
  return find(db, exports.tribeInvitationsPath, id)
}

/**
 * @param db
 * @param {String} id
 * @returns {Null|Object}
 */
exports.findTribe = async (db, id) => {
  return find(db, exports.tribesPath, id)
}

/**
 * @param db
 * @param {String} id
 * @returns {Promise}
 */
exports.findFriendRequest = async (db, id) => {
  return find(db, exports.friendRequestsPath, id)
}

/**
 * @param db
 * @param {String} id
 * @param {String} iid
 * @returns {Promise}
 */
exports.addTribePendingInvitations = async (db, id, iid) => {
  if (!id) return
  return db.ref(exports.tribesPath).child(id).child('pendingInvitations').child(iid).set(true)
}

/**
 * @param db
 * @param {String} id
 * @param {String} iid
 * @returns {Promise | Null}
 */
exports.removeTribePendingInvitations = (db, id, iid) => {
  if (!id) return
  return db.ref(exports.tribesPath).child(id).child('pendingInvitations').child(iid).set(null)
}

/**
 * Add user's invitation
 * @param db
 * @param uid
 * @param iid
 * @returns {void}
 */
exports.addUserInvitation = (db, uid, iid) => {
  if (!uid) return

  db.ref(exports.usersPath)
    .child(uid)
    .child('private')
    .child('invitations')
    .child(iid)
    .set(true)
}

/**
 * Disables user's invitation
 * @param db
 * @param uid
 * @param iid
 * @returns {void}
 */
exports.disableUserInvitation = (db, uid, iid) => {
  if (!uid) return

  db.ref(exports.usersPath)
    .child(uid)
    .child('private')
    .child('invitations')
    .child(iid)
    .set(false)
}

/**
 * Add user's sent friend requests
 * @param db
 * @param uid
 * @param fid
 * @returns {void}
 */
exports.addUserSentFriendRequests = (db, uid, fid) => {
  if (!uid) return

  db.ref(exports.usersPath)
    .child(uid)
    .child('private')
    .child('sentFriendRequests')
    .child(fid)
    .set(true)
}

/**
 * Disable user's sent friend requests
 * @param db
 * @param uid
 * @param fid
 * @returns {void}
 */
exports.disableUserSentFriendRequests = (db, uid, fid) => {
  if (!uid) return

  db.ref(exports.usersPath)
    .child(uid)
    .child('private')
    .child('sentFriendRequests')
    .child(fid)
    .set(false)
}

/**
 * Add user's received friend requests
 * @param db
 * @param uid
 * @param fid
 * @returns {void}
 */
exports.addUserReceivedFriendRequests = (db, uid, fid) => {
  if (!fid || !uid) return

  return db.ref(exports.usersPath)
    .child(uid)
    .child('private')
    .child('receivedFriendRequests')
    .child(fid)
    .set(true)
}

/**
 * Disables user's received friend requests
 * @param db
 * @param uid
 * @param fid
 * @returns {void}
 */
exports.disableUserReceivedFriendRequests = (db, uid, fid) => {
  if (!uid) return

  db.ref(exports.usersPath)
    .child(uid)
    .child('private')
    .child('receivedFriendRequests')
    .child(fid)
    .set(false)
}

/**
 * updates user tribeId
 * @param {Object} db
 * @param {String} uid
 * @param {String} tribeId
 */
exports.updateUserTribe = (db, uid, tribeId) => {
  db.ref(exports.usersPath)
    .child(uid)
    .child('private')
    .update({tribeId: tribeId})
}

/**
 * Updates user with attrs
 * @param {Object} db
 * @param {String} uid
 * @param {Object} attrs
 */
exports.updateUser = async (db, uid, attrs) => {
  db.ref(exports.usersPath)
    .child(uid)
    .update(attrs)
}

/**
 * Gets default user device token
 * @param {Object} user
 * @param {String} device
 * @return {String}
 */
exports.userDeviceId = (user, device) => {
  if (device === 'ANDROID') {
    return user.private.androidDeviceToken
  }

  return user.private.appleDeviceToken
}

/**
 * Gets user location
 * @param user
 * @returns {{latitude: number, longitude: *}}
 */
exports.userLocation = (user) => {
  return {
    latitude: user.private.latitude,
    longitude: user.private.longitude
  }
}

/**
 * Gets default user email
 * @param {Object} user
 * @return {String}
 */
exports.userEmail = (user) => {
  return user.private.email
}

/**
 * Gets default user userName
 * @param {Object} user
 * @return {String}
 */
exports.userUserName = (user) => {
  return user.public.userName
}


/**
 * Gets user's displayName

 * @return {String}
 */
exports.userDisplayName = (user) => {
  return user.public.displayName
}

/**
 * Get user's tribeId
 * @param {Object} user
 * @returns {String}
 */
exports.userTribeId = (user) => {
  return user.private.tribeId
}

/**
 * Gets user twilio access token
 * @param {Object} user
 * @return {String}
 */
exports.userTwilioAccessToken = (user) => {
  return user.private.twilioAccessToken
}

/**
 * Gets user radius
 * @param {Object} user
 * @return {Number}
 */
exports.userRadius = (user) => {
  return user.private.radius
}

/**
 * Gets user connection status
 * @param {Object} user
 * @return {String}
 */
exports.userConnectionStatus = (user) => {
  return user.private.connectionStatus
}

/**
 * Gets lastUpdateLocationTime
 * @param {Object} user
 * @return {Number}
 */
exports.userLastUpdateLocationTime = (user) => {
  return user.private.lastUpdateLocationTime
}

/**
 * Gets user friend id
 * @param {Object} user
 * @return {String}
 */
exports.userFriendId = (user) => {
  return user.public.friendId
}

/**
 * Return user id from friend id
 * @param {Object} db
 * @param {String} friendId
 * @returns {Null|Object}
 */
exports.findFriendId = async (db, friendId) => {
  return find(db, exports.friendIdsPath, friendId)
}

/**
 * creates new tribe
 * @param db
 * @param {Object} attrs
 * @returns {String} tribeId
 */
exports.createTribe = (db, attrs) => {
  const newTribe = db.ref(exports.tribesPath).push()
  newTribe.set(attrs)
  return newTribe.key
}

/**
 * general error logging
 * @param {String} objectName
 * @param {String} id
 */
const errorMissing = (objectName, id) => {
  log.error(`${objectName} with id: ${id} is not found.`)
}

/**
 * Log missing user error message
 * @param {String} userId
 */
exports.errorMissingUser = (userId) => {
  log.error(`User with id: ${userId} is not found.`)
}

/**
 * Log missing tribe error message
 * @param {String} tribeId
 */
exports.errorMissingTribe = (tribeId) => {
  log.error(`Tribe with id: ${tribeId} is not found.`)
}

exports.errorMissingFriendRequest = (id) => {
  errorMissing('FriendRequest', id)
}

exports.errorMissingTribeInvitation = (id) => {
  errorMissing('TribeInvitation', id)
}
