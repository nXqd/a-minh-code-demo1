const R = require('ramda')
const log = require('winston')
const shared = require('./shared')
const userd = require('./domain/user')

/**
 * Friend request payload
 * @param {String} userDisplayName
 * @returns {{notification: {title: string, body: string, sound: string}}}
 */
exports.joinTribePayload = (userDisplayName) => {
  return {
    notification: {
      title: 'Member quits tribe.',
      body: `${userDisplayName} has joined the tribe.`,
      sound: 'default'
    }
  }
}

/**
 * updates tribe invitation
 * @param db
 * @param {String} iid
 * @param {String} status
 */
exports.updateTribeInvitation = (db, iid, status) => {
  const statusUppercase = status.toUpperCase()
  db.ref(shared.tribeInvitationsPath).child(iid).child('status').set(statusUppercase)
}

/**
 * adds user to the tribe
 * @param db
 * @param {String} uid
 * @param {String} tid
 */
exports.addUserToTribe = (db, uid, tid) => {
  db.ref(shared.tribesPath).child(tid).child('members').child(uid).set(true)
}

/**
 * Accepts tribe's invitation
 * - Update database
 * - Send notification to other tribe members
 * @param admin
 * @param db
 * @param {String} uid
 * @param {String} tribeId
 */
const acceptTribeInvitation = async (admin, db, uid, tribeId) => {
  shared.updateUserTribe(db, uid, tribeId)
  exports.addUserToTribe(db, uid, tribeId)

  const user = await shared.findUser(db, uid)
  if (R.isNil(user)) {
    shared.errorMissingUser(uid)
    return
  }

  const userDisplayName =  shared.userDisplayName(user)
  const messagePayload = exports.joinTribePayload(userDisplayName)
  userd.sendMessageToTribeMembers(admin, db, uid, messagePayload)
}

/**
 * @param admin
 * @param db
 * @param {Object} data
 */
exports.updateTribeInvitationAPI = async (admin, db, data) => {
  const {uid, iid, action} = data

  const invitation = await shared.findInvitation(db, iid)
  if (R.isNil(invitation)) {
    log.error(shared.errorMissingTribeInvitation(iid))
    return
  }

  exports.updateTribeInvitation(db, iid, action)
  const tribeId = invitation.tribeId

  if (action === "ACCEPT") {
    acceptTribeInvitation(admin, db, uid, tribeId)
  }

  shared.removeTribePendingInvitations(db, invitation.tribeId, iid)
  shared.disableUserInvitation(db, uid, iid)
}
