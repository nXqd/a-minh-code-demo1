const R = require('ramda')
const itwilio = require('./infra/twilio')
const shared = require('./shared')
const log = require('winston')

/**
 * updates user's twilio token
 * @param {Object} db
 * @param {String} uid
 * @param {String} accessToken
 * @returns {Boolean}
 */
const updateUserTwilioAccessToken = async (db, uid, accessToken) => {
  try {
    db.ref(shared.usersPath)
      .child(uid)
      .child('private')
      .update({
        twilioAccessToken: accessToken
      })
    return true
  } catch (err) {
    log.error(`Error while updating user's username: ${err}`)
    return false
  }
}

/**
 * Refreshes twilio access token
 * @param db
 * @param data
 * @returns {Promise.<void>}
 */
exports.refreshTwilioAccessTokenAPI = async (db, data) => {
  const {uid, device} = data

  const user = await shared.findUser(db, uid)
  if (R.isNil(user)) {
    shared.errorMissingUser(uid)
    return
  }

  if (device === 'APPLE') {
    const token = await itwilio.generateAccessTokenForUser(user, device)
    if (!token) {
      log.error(`Token cannot be generated.`)
      return
    }
    updateUserTwilioAccessToken(db, uid, token)
  }
}
