const R = require('ramda')
const log = require('winston')
const moment = require('moment')
const shared = require('./shared')
const userd = require('./domain/user')
const utils = require('./utils/index')

/**
 * @param deg
 * @returns {number}
 */
const deg2rad = (deg) => {
  return deg * (Math.PI / 180)
}

/**
 * return distance in KM between 2 lat-long pairs
 * @param lat1
 * @param lon1
 * @param lat2
 * @param lon2
 * @returns {number} distance between latlon in km
 */
const distanceFromLatLongInKM = (lat1, lon1, lat2, lon2) => {
  const r = 6371 // Radius of the earth in KM
  const dLat = deg2rad(lat2 - lat1)  // deg2rad below
  const dLon = deg2rad(lon2 - lon1)
  const a =
    Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
    Math.sin(dLon / 2) * Math.sin(dLon / 2)
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
   // Distance in KM
  return r * c
}

/**
 * check if lat lon within radius in km
 * @param {Number} lat1
 * @param {Number} lon1
 * @param {Number} lat2
 * @param {Number} lon2
 * @param {Number} radiusInKm
 * @returns {Null|Boolean}
 */
const isWithinRadius = (lat1, lon1, lat2, lon2, radiusInKm) => {
  if (R.isNil(radiusInKm)) {
    log.error("Missing radius")
    return null
  }

  return (distanceFromLatLongInKM(lat1, lon1, lat2, lon2) <= radiusInKm)
}

/**
 * check if lat lon within 500m
 * @param {Number} lat1
 * @param {Number} lon1
 * @param {Number} lat2
 * @param {Number} lon2
 * @returns {Boolean|boolean}
 */
const isWithin500m = (lat1, lon1, lat2, lon2) => {
  return isWithinRadius(lat1, lon1, lat2, lon2, 0.5)
}

/**
 * check if lat lon within 250m
 * @param {Number} lat1
 * @param {Number} lon1
 * @param {Number} lat2
 * @param {Number} lon2
 * @returns {Boolean|boolean}
 */
const isWithin250m = (lat1, lon1, lat2, lon2) => {
  return isWithinRadius(lat1, lon1, lat2, lon2, 0.25)
}

/**
 * checks if user is just out of radius
 * @param {Object} prevLocation
 * @param {Object} location
 * @param {Object} user2Location
 * @param {Number} radiusInKm
 * @returns {Boolean|boolean}
 * @constructor
 */
const isUserOutOfRadius = (prevLocation, location, user2Location, radiusInKm) => {
  if (R.isNil(radiusInKm)) {
    log.error("Missing radius")
    return null
  }

  return (
    // isWithin500m(prevLocation.latitude, prevLocation.longitude, user2Location.latitude, user2Location.longitude) &&
    // !isWithin500m(location.latitude, location.longitude, user2Location.latitude, user2Location.longitude)
    // isWithin250m(prevLocation.latitude, prevLocation.longitude, user2Location.latitude, user2Location.longitude) &&
    // !isWithin250m(location.latitude, location.longitude, user2Location.latitude, user2Location.longitude)

    isWithinRadius(prevLocation.latitude, prevLocation.longitude, user2Location.latitude, user2Location.longitude, radiusInKm) &&
    !isWithinRadius(location.latitude, location.longitude, user2Location.latitude, user2Location.longitude, radiusInKm)
  )
}

/**
 * checks if user is just within of radius
 * @param {Object} prevLocation
 * @param {Object} location
 * @param {Object} user2Location
 * @param {Number} radiusInKm
 * @returns {Boolean|boolean}
 * @constructor
 */
const isUserWithinRadius = (prevLocation, location, user2Location, radiusInKm) => {
  if (R.isNil(radiusInKm)) {
    log.error("Missing radius")
    return null
  }

  return (
    // !isWithin500m(prevLocation.latitude, prevLocation.longitude, user2Location.latitude, user2Location.longitude) &&
    // isWithin500m(location.latitude, location.longitude, user2Location.latitude, user2Location.longitude)
    // !isWithin250m(prevLocation.latitude, prevLocation.longitude, user2Location.latitude, user2Location.longitude) &&
    // isWithin250m(location.latitude, location.longitude, user2Location.latitude, user2Location.longitude)

    !isWithinRadius(prevLocation.latitude, prevLocation.longitude, user2Location.latitude, user2Location.longitude, radiusInKm) &&
    isWithinRadius(location.latitude, location.longitude, user2Location.latitude, user2Location.longitude, radiusInKm)
  )
}

/**
 * Tribe member is connected payload.
 * @param {String} userDisplayName
 * @returns {{notification: {title: string, body: string, sound: string}}}
 */
exports.tribeMemberConnectedPayload = (userDisplayName) => {
  const prefix = userDisplayName === 'You' ? 'You have ' : `${userDisplayName} has `

  return {
    notification: {
      title: `A tribe member has reconnected.`,
      body: `${prefix} reconnected.`,
      sound: 'default'
    }
  }
}

/**
 * Tribe member is disconnected payload.
 * @param {String} userDisplayName
 * @returns {{notification: {title: string, body: string, sound: string}}}
 */
exports.tribeMemberDisconnectedPayload = (userDisplayName) => {
  const prefix = userDisplayName === 'You' ? 'You have ' : `${userDisplayName} has `

  return {
    notification: {
      title: `A tribe member has disconnected.`,
      body: `${prefix} disconnected.`,
      sound: 'default'
    }
  }
}

/**
 * Updates user location
 * - send remote notification if the last status is null or DISCONNECTED
 * @param {Object} admin
 * @param {Object} db
 * @param {string} uid
 * @param {{latitude: number, longitude: *}} location
 * @param {string} lastConnectionStatus
 * @param {string} userDisplayName
 */
const updateUserLocation = (admin, db, uid, location, lastConnectionStatus, userDisplayName) => {
  if (lastConnectionStatus !== "CONNECTED") {
    const payload =  exports.tribeMemberConnectedPayload(userDisplayName)
    userd.sendMessageToTribeMembers(admin, db, uid, payload)

    shared.sendMessage(admin, db, uid, exports.tribeMemberConnectedPayload("You"))
  }

  userd.updateUserLocation(db, uid, location)
}

/**
 * creates out of radius payload
 * @param userDisplayName
 * @returns {{notification: {title: string, body: string, sound: string}}}
 */
exports.outOfRadiusPayload = (userDisplayName) => {
  return {
    notification: {
      title: 'Out-of-Radius Alert',
      body: `Member ${userDisplayName} have gone out of your radius.`,
      sound: 'default'
    }
  }
}

/**
 * creates within radius payload
 * @param userDisplayName
 * @returns {{notification: {title: string, body: string, sound: string}}}
 */
exports.withinRadiusPayload = (userDisplayName) => {
  return {
    notification: {
      title: 'Within-Radius Alert',
      body: `Member ${userDisplayName} have gone out of your radius.`,
      sound: 'default'
    }
  }
}

/**
 * Minutes to now
 * @param {Number} start
 * @returns {number} minutes to now
 */
const minutesToNow = (start) => {
  const now = moment(new Date())
  const _start = moment(start)
  const duration = moment.duration(_start.diff(now))
  return duration.asMinutes()
}

/**
 * Updates user status to DISCONNECTED
 * if he/she doesn't update his/her location in 20 minutes and his/her status is CONNECTED.
 * @param db
 * @param {String} uid
 * @param {Object} user
 * @returns {Null | Promise.<void>}
 */
const updateTribeMemberStatus = async (db, uid, user) => {
  const connectionStatus = shared.userConnectionStatus(user)
  if (connectionStatus === "DISCONNECTED") {
    return null
  }

  const lastUpdateLocationTime = shared.userLastUpdateLocationTime(user)
  if (minutesToNow(lastUpdateLocationTime) < 20) {
    return null
  }

  userd.updateUserConnectionStatus(db, uid, "DISCONNECTED")
  shared.sendMessage(admin, db, uid, exports.tribeMemberDisconnectedPayload("You"))
  userd.sendMessageToTribeMembers(admin, db, uid, exports.tribeMemberDisconnectedPayload(shared.userDisplayName(user)))
}

/**
 * update location API
 * @param admin
 * @param db
 * @param data
 */
exports.updateLocationAPI = async (admin, db, data) => {
  const {uid, location} = data

  const user = await shared.findUser(db, uid)
  if (R.isNil(user)) {
    shared.errorMissingUser(uid)
    return
  }

  const tribeId = shared.userTribeId(user)
  const tribe = await shared.findTribe(db, tribeId)
  if (R.isNil(tribe)) {
    shared.errorMissingTribe(tribeId)
    return
  }

  const {members} = tribe
  const userLocation = shared.userLocation(user)
  let user2Location

  const membersIds = Object.keys(members)
  const dynamicradiusInKm = utils.mToKm(shared.userRadius(user))
  if (R.isNil(dynamicradiusInKm)) {
    log.error('User radius is missing!')
    return
  }

  const lastConnectionStatus = shared.userConnectionStatus(user)
  const userDisplayName = shared.userDisplayName(user)
  updateUserLocation(admin, db, uid, location, lastConnectionStatus, userDisplayName)

  R.map(
    // Send messages to another users regarding the new location
    async (uid2) => {
      // Dont update the current user
      if (uid2 === uid) return

      const user2 = await shared.findUser(db, uid2)
      if (R.isNil(user)) {
        shared.errorMissingUser(uid2)
        return
      }

      user2Location = shared.userLocation(user2)
      if (isUserOutOfRadius(userLocation, location, user2Location, dynamicradiusInKm)) {
        shared.sendMessage(admin, db, uid2, exports.outOfRadiusPayload(shared.userDisplayName(user)))
        return
      }

      if (isUserWithinRadius(userLocation, location, user2Location, dynamicradiusInKm)) {
        shared.sendMessage(admin, db, uid2, exports.withinRadiusPayload(shared.userDisplayName(user)))
      }

      // Update tribe member's status
      updateTribeMemberStatus(db, uid2, user2)
    },
    membersIds
  )
}
