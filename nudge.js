const R = require('ramda')
const log = require('winston')
const shared = require('./shared')
const userd = require('./domain/user')

/**
 * creates nudge payload
 * @param {String} senderName
 * @returns {{notification: {title: string, body: string, sound: string}}}
 */
exports.nudgePayload = (senderName) => {
  return {
    notification: {
      title: 'Nudge',
      body: `${senderName} has nudged you!`,
      sound: 'default'
    }
  }
}

/**
 * Nudges user API
 * @param admin
 * @param db
 * @param data
 */
exports.nudgeAPI = async (admin, db, data) => {
  const {uid} = data

  const user = await shared.findUser(db, uid)
  if (R.isNil(user)) {
    shared.errorMissingUser(uid)
    return
  }

  const nudgePayload = exports.nudgePayload(shared.userDisplayName(user))
  userd.sendMessageToTribeMembers(admin, db, uid, nudgePayload)
}
