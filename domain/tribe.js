const R = require('ramda')
const itwilio = require('../infra/twilio')
const log = require('winston')
const shared = require('../shared')

/**
 * Updates tribe with attrs
 * @param {Object} db
 * @param {String} tid
 * @param {Object} attrs
 */
exports.update = async (db, tid, attrs) => {
  db.ref(shared.tribesPath)
    .child(tid)
    .update(attrs)
}

/**
 * Creates new firebase tribe, new tribe's channel
 * @param {Object} db
 * @param {Object} twilioChatService
 * @param {String} tribeName
 * @returns {Promise.<[String,Object]>}
 */
exports.createTribe = async (db, twilioChatService, tribeName) => {
  let tribeId, newTribeChannel
  try {
    tribeId = shared.createTribe(db, {tribeName: tribeName})
    newTribeChannel = await itwilio.createChannel(twilioChatService, tribeId)
    exports.update(db, tribeId, {twilioChannelSid: newTribeChannel.sid})
  } catch (e) {
    log.error(`There is problem while creating new tribe: ${e}`)
    return
  }

  return [tribeId, newTribeChannel]
}

/**
 * joins tribe
 * @param db
 * @param {String} tid
 * @param {String} uid
 */
exports.addTribeMember = (db, tid, uid) => {
  db.ref(shared.tribesPath)
    .child(tid)
    .child('members')
    .child(uid)
    .set(true)
}
