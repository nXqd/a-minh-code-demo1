const R = require('ramda')
const itwilio = require('../infra/twilio')
const log = require('winston')
const shared = require('../shared')

/**
 * Add user to channel and updates user with new twilioUserSID
 * @param {Object} db
 * @param {Object} channel
 * @param {Object} user
 * @param {String} uid
 * @param user
 */
exports.addUserToChannel = async (db, channel, user, uid) => {
  const twilioMember = await itwilio.addUserToChannel(channel, itwilio.userIdentity(user))
  shared.updateUser(db, uid, {twilioUserSID: twilioMember.sid})
}

/**
 * Updates location
 * - update new location ( lang, lot )
 * - update connection status to CONNECTED
 * - update lastUpdateLocationTime to Date.now()
 * @param {Object} db
 * @param {string} uid
 * @param {{latitude: number, longitude: *}} location
 */
exports.updateUserLocation = (db, uid, location) => {
  const _updateObj = location
  _updateObj.connectionStatus = "CONNECTED"
  _updateObj.lastUpdateLocationTime = Date.now()

  db.ref(shared.usersPath)
    .child(uid)
    .child('private')
    .update(_updateObj)
}

/**
 * Updates user connection status
 * @param {Object} db
 * @param {string} uid
 * @param {string} connectionStatus
 */
exports.updateUserConnectionStatus = (db, uid, connectionStatus) => {
  db.ref(shared.usersPath)
    .child(uid)
    .child('private')
    .update({connectionStatus: connectionStatus})
}

/**
 * quits tribe
 * @param db
 * @param {String} tid - Tribe's id
 * @param {String} uid
 */
exports.quitTribe = (db, tid, uid) => {
  db.ref(shared.tribesPath)
    .child(tid)
    .child('members')
    .child(uid)
    .set(null)
}

/**
 * Send message to other tribe members
 * @param {Object} admin
 * @param {Object} db
 * @param {String} uid
 * @param {{notification: {title: string, body: string, sound: string}}} messagePayload
 * @returns {Promise.<void>}
 */
exports.sendMessageToTribeMembers = async (admin, db, uid, messagePayload) => {
  const {members} = await shared.findTribe(db, uid)
  if (R.isNil(members) || R.isEmpty(members)) {
    log.warn(`Tribe members is empty.`)
    return
  }

  R.map(
    async (id) => {
      if (id === uid) return
      shared.sendMessage(admin, db, id, messagePayload)
    },
    Object.keys(members)
  )
}
