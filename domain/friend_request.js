const shared = require('../shared')

/**
 * creates friend request with referencing friend ID
 * @param db
 * @param {String} requesterUid
 * @param {String} requesteeUid
 * @param {String} friendId
 */
const createFriendRequestWithFriendId = (db, requesterUid, requesteeUid, friendId) => {
  const newFriendRequest = db.ref('/data/friendRequests').push()
  newFriendRequest.set({
    'requesterUid': requesterUid,
    'requesteeUid': requesteeUid,
    'friendId': friendId
  })

  return newFriendRequest.key
}

/**
 * creates friend request
 * @param db
 * @param {String} requesterUid
 * @param {String} requesteeUid
 */
const createFriendRequest = (db, requesterUid, requesteeUid) => {
  const newFriendRequest = db.ref('/data/friendRequests').push()
  newFriendRequest.set({
    'requesterUid': requesterUid,
    'requesteeUid': requesteeUid
  })

  return newFriendRequest.key
}

/**
 * Add user's sent friend requests
 * @param db
 * @param uid
 * @param fid
 * @returns {void}
 */
exports.addUserSentFriendRequests = (db, uid, fid) => {
  if (!uid) return

  db.ref(shared.usersPath)
    .child(uid)
    .child('private')
    .child('sentFriendRequests')
    .child(fid)
    .set(true)
}

/**
 * Add user's received friend requests
 * @param db
 * @param uid
 * @param fid
 * @returns {void}
 */
exports.addUserReceivedFriendRequests = (db, uid, fid) => {
  if (!fid || !uid) return

  return db.ref(shared.usersPath)
    .child(uid)
    .child('private')
    .child('receivedFriendRequests')
    .child(fid)
    .set(true)
}

/**
 * Create new friend request
 * @param {Object} db
 * @param {String} requesterUid
 * @param {String} requesteeUid
 */
exports.createFriendRequest  = (db, requesterUid, requesteeUid) => {
  const fid = createFriendRequest(db, requesterUid, requesteeUid)
  exports.addUserSentFriendRequests(db, requesterUid, fid)
  exports.addUserReceivedFriendRequests(db, requesteeUid, fid)
}

/**
 * Create new friend request with referencing friend ID
 * @param {Object} db
 * @param {String} requesterUid
 * @param {String} requesteeUid
 * @param {String} friendId
 */
exports.createFriendRequestWithFriendId = (db, requesterUid, requesteeUid, friendId) => {
  const fid = createFriendRequestWithFriendId(db, requesterUid, requesteeUid, friendId)
  exports.addUserSentFriendRequests(db, requesterUid, fid)
  exports.addUserReceivedFriendRequests(db, requesteeUid, fid)
}
