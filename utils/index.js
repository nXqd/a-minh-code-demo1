const R = require('ramda')
const shared = require('../shared')
const log = require('winston')

/**
 * Convert m to km
 * @param x
 * @returns {Null|Number}
 */
exports.mToKm = (x) => {
  if (!shared.isValidNumber(x)) {
    log.warn("Input is not a valid number!")
    return null
  }

  return x / 1000
}
