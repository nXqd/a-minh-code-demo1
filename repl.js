const Queue = require('firebase-queue')
const admin = require('firebase-admin')
const config = require('config')
const Twilio = require('twilio').Twilio;
const itwilio = require('./infra/twilio')

/**
 * creates twilio client
 * @param accountSID
 * @param authToken
 * @returns {*}
 */
const createTwilioClient = (accountSID, authToken) => {
  return new Twilio(accountSID, authToken)
}

const twilioClient = createTwilioClient(config.get('twilio.accountSID'), config.get('twilio.authToken'))

// const a = createChatClient()
// a.then((res) => (console.log(res)))


// const service = messagingClient.get(config.get('twilio.chatServiceSID'));

const chatService = itwilio.createChatService(twilioClient, config.get('twilio.chatServiceSID'))

const b = async () => {
  const p = await itwilio.createChannel(chatService, "Unique11", "Friendly")
  const p2 = await itwilio.addUserToChannel(p, "abc@gmail.com")
  const p3 = await itwilio.removeUserFromChannel(chatService, p.sid, p2.sid)
  // const p4 = await p.remove()
  console.log(p.sid)
  console.log(p2)
  console.log(p3)
  // console.log(p4)
}

b()

// const twilioChatService = createTwilioService(twilioClient)

// const refreshTwilioAccessTokenAPI = require('./src/refresh_twilio_access_token')


// itwilio.createChannel(service, "test", "test")


