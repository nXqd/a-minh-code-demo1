const R = require('ramda')
const fr = require('./domain/friend_request')
const shared = require('./shared')

/**
 * Friend request payload
 * @param {String} body
 * @returns {{notification: {title: string, body: string, sound: string}}}
 */
exports.friendRequestPayload = (body) => {
  return {
    notification: {
      title: 'Friend request sent.',
      body: body,
      sound: 'default'
    }
  }
}

/**
 * Friend request received payload
 * @returns {{notification: {title: string, body: string, sound: string}}}
 */
exports.friendRequestReceivedPayload = {
  return {
    notification: {
      title: 'Friend request received.',
      body: `You have received a friend invitation.`,
      sound: 'default'
    }
  }
}

/**
 * Creates friend request
 * - Create friend request
 * - Send remote notification to the current user
 * @param admin
 * @param db
 * @param uid
 * @param requesteeUid
 */
const createFriendRequest = async (admin, db, uid, requesteeUid) => {
  const requestee = await shared.findUser(db, requesteeUid)

  if (R.isNil(requestee)) {
    shared.errorMissingUser(requesteeUid)
    return
  }

  fr.createFriendRequest(db, uid, requesteeUid)

  shared.sendMessage(admin, db, uid, exports.friendRequestPayload(`Friend request to ${shared.userDisplayName(requestee)} sent.`))
  shared.sendMessage(admin, db, requesteeUid, exports.friendRequestReceivedPayload)
}

/**
 * sends friend request api
 * @param admin
 * @param db
 * @param data
 */
exports.sendFriendRequestAPI = async (admin, db, data) => {
  const {uid, requesteeUid} = data

  createFriendRequest(admin, db, uid, requesteeUid)
}
