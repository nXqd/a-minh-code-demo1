const shared = require('./shared')
const R = require('ramda')

/**
 * @param db
 * @param {String} fid
 * @param {String} status
 */
const updateFriendRequestStatus = (db, fid, status) => {
  const statusLowercase = status.toUpperCase()
  db.ref(shared.friendRequestsPath).child(fid).child('status').set(statusLowercase)
}

/**
 * creates friendship payload
 * @param receiverName
 * @returns {{notification: {title: string, body: string, sound: string}}}
 */
exports.friendshipPayload = (receiverName) => {
  return {
    notification: {
      title: 'Friendship',
      body:  `You and ${receiverName} are now friends.`,
      sound: 'default'
    }
  }
}

/**
 * rejects friendship payload
 * @param {String} body
 * @returns {{notification: {title: string, body: string, sound: string}}}
 */
exports.rejectFriendshipPayload = (body) => {
  return {
    notification: {
      title: 'Friendship rejection',
      body: body,
      sound: 'default'
    }
  }
}

/**
 * creates friendship data record
 * @param db
 * @param {String} fid
 * @param {String} uid1
 * @param {String} uid2
 */
const createFriendShip = (db, fid, uid1, uid2) => {
  db.ref(shared.friendShipsPath)
    .child(uid1)
    .child(uid2)
    .set(true)

  db.ref(shared.friendShipsPath)
    .child(uid2)
    .child(uid1)
    .set(true)
}

/**
 * accepts friend ship
 * - send remote notification to each other
 * @param admin
 * @param db
 * @param {String} fid
 * @param {String} requesterUid
 * @param {String} requesteeUid
 * @returns {Promise.<void>}
 */
const acceptFriendship = async (admin, db, fid, requesterUid, requesteeUid) => {
  updateFriendRequestStatus(db, fid, 'ACCEPTED')
  const requester = await shared.findUser(db, requesterUid)

  if (R.isNil(requester)) {
    shared.errorMissingUser(requesterUid)
    return
  }

  const requestee = await shared.findUser(db, requesteeUid)

  if (R.isNil(requestee)) {
    shared.errorMissingUser(requesteeUid)
    return
  }

  createFriendShip(db, fid, requesteeUid, requesterUid)

  shared.sendMessage(admin, db, requesterUid, exports.friendshipPayload(shared.userDisplayName(requestee)))
  shared.sendMessage(admin, db, requesteeUid, exports.friendshipPayload(shared.userDisplayName(requester)))
}

/**
 *
 * @param admin
 * @param db
 * @param {String} fid
 * @param {String} requesterUid
 * @param {String} requesteeUid
 * @returns {Promise.<void>}
 */
const rejectFriendship = async (admin, db, fid, requesterUid, requesteeUid) => {
  updateFriendRequestStatus(db, fid, 'REJECTED')

  const requester = await shared.findUser(db, requesterUid)

  if (R.isNil(requester)) {
    shared.errorMissingUser(requesterUid)
    return
  }

  const requestee = await shared.findUser(db, requesteeUid)

  if (R.isNil(requestee)) {
    shared.errorMissingUser(requesteeUid)
    return
  }

  shared.sendMessage(admin, db, requesterUid,
    exports.rejectFriendshipPayload(`${shared.userDisplayName(requestee)} has rejected your friendship invitation.`))
  shared.sendMessage(admin, db, requesteeUid,
    exports.rejectFriendshipPayload(`You have rejected ${shared.userDisplayName(requester)} friendship invitation.`))
}

/**
 * updates friend request based on received ACTION
 * @param admin
 * @param db
 * @param {Object} data
 */
exports.updateFriendRequestAPI = async (admin, db, data) => {
  const {fid, action} = data

  const friendRequest = await shared.findFriendRequest(db, fid)
  if (R.isNil(friendRequest)) {
    shared.errorMissingFriendRequest(friendRequest)
    return
  }

  const {requesterUid, requesteeUid} = friendRequest

  if (action === 'ACCEPT') {
    acceptFriendship(admin, db, fid, requesterUid, requesteeUid)
  } else if (action === 'REJECT') {
    rejectFriendship(admin, db, fid, requesterUid, requesteeUid)
  }

  shared.disableUserSentFriendRequests(db, requesterUid, fid)
  shared.disableUserReceivedFriendRequests(db, requesteeUid, fid)
}
