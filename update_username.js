const R = require('ramda')
const log = require('winston')
const shared = require('./shared')

/**
 * Checks username's uniqueness
 * @param db
 * @param {String} username
 * @returns {Boolean}
 */
const isUsernameUnique = async (db, username) => {
  if (!username) return false

  let snapshot
  try {
    snapshot = await db.ref(exports.usersPath)
      .child('usernames')
      .child(username)
      .once('value')
  } catch (err) {
    log.error('Error in looking up for username.')
  }
  if (R.isNil(snapshot)) {
    return true
  }
  const val = snapshot.val()
  if (val === false) {
    return true
  }

  return false
}

/**
 * updates user's name
 * @param db
 * @param {String} uid
 * @param {String} username
 * @returns {Boolean}
 */
const updateUserName = async (db, uid, username) => {
  try {
    db.ref(shared.usersPath)
      .child(uid)
      .child('public')
      .update({
        username: username
      })
    return true
  } catch (err) {
    log.error(`Error while updating user's username: ${err}`)
    return false
  }
}

/**
 * Add username to usernames list
 * @param db
 * @param {String} uid
 * @param {String} username
 * @returns {Boolean}
 */
const addUsernameMeta = async (db, username) => {
  db.ref(shared.usersPath)
    .child('usernames')
    .child(username)
    .set(true)
}

/**
 * Removes username out of usernames list
 * @param db
 * @param {String} uid
 * @param {String} username
 * @returns {Boolean}
 */
const removeUsernameMeta = async (db, username) => {
  db.ref(shared.usersPath)
    .child('usernames')
    .child(username)
    .set(null)
}

/**
 * Updates user's username
 * @param db
 * @param data
 * @returns {Promise.<void>}
 */
exports.updateUsernameAPI = async (db, data) => {
  const {uid, username} = data

  const user = await shared.findUser(db, uid)
  const oldUsername = user.public.username
  if (oldUsername === username) {
    log.warn(`User's username and new username is the same.`)
    return
  }

  const isUniqueUsername = await isUsernameUnique(db, username)
  if (!isUniqueUsername) {
    log.warn(`User's username ${username} is being used.`)
    return
  }

  let ok = updateUserName(db, uid, username)
  if (ok) {
    removeUsernameMeta(db, oldUsername)
    addUsernameMeta(db, username)
  }
}
