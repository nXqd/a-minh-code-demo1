const R = require('ramda')
const log = require('winston')
const shared = require('./shared')

/**
 * Check if radius is a valid number
 * @param {Number} x
 */
const isValidRadius = (x) => {
  return (x > 0)
}


/**
 * Update user's radius
 * @param {Object} db
 * @param {String} uid
 * @param {Number} radius
 * @returns {boolean}
 */
const updateUserRadius = (db, uid, radius) => {
  try {
    db.ref(shared.usersPath)
      .child(uid)
      .child('private')
      .update({
        radius: radius
      })
    return true
  } catch (err) {
    log.error(`Error while updating user's radius: ${err}`)
    return false
  }
}

/**
 * Updates user's radius
 * @param db
 * @param data
 * @returns {Promise.<void>}
 */
exports.updateUserRadiusAPI = async (db, data) => {
  const {uid, radius} = data

  if (!isValidRadius(radius)) {
    log.error('Radius is not valid')
    return
  }

  updateUserRadius(db, uid, radius)
}
