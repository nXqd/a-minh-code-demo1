const R = require('ramda')
const itwilio = require('./infra/twilio')
const shared = require('./shared')
const duser = require('./domain/user')
const log = require('winston')

/**
 * Updates tribe's Twilio Channel SID
 * @param db
 * @param {string} tribeId
 * @param {String} sid
 */
const updateTribeTwilioChannelSID = (db, tribeId, sid) => {
  db.ref(shared.tribesPath)
    .child(tribeId)
    .update({twilioChannelSid: sid})
}

/**
 * @param db
 * @param {Object} twilioChatService
 * @param {Object} data
 */
exports.setTwilioChannelSidForSelfTribeAPI = async (db, twilioChatService, data) => {
  const {uid} = data

  const user = await shared.findUser(db, uid)
  if (R.isNil(user)) {
    shared.errorMissingUser(uid)
    return
  }

  const tribeId = await shared.userTribeId(user)
  if (R.isNil(tribeId)) {
    log.error('User\'s tribeid is missing.')
    return
  }

  const tribe = await shared.findTribe(db, tribeId)
  if (R.isNil(tribeId)) {
    shared.errorMissingTribe(tribeId)
    return
  }

  const tribeSID = tribe.twilioChannelSid
  if (!R.isEmpty(tribeSID) && !R.isNil(tribeSID)) {
    log.warn("This tribe already has a twilioChannelSid.")
    return
  }

  // itwilio.removeChannel(twilioChatService, tribe.tribeName)
  const channel = await itwilio.createChannel(twilioChatService, tribeId, tribe.tribeName)
  if (R.isNil(channel)) {
    log.error(`Channel with id ${tribeId} is not found.`)
    return
  }

  updateTribeTwilioChannelSID(db, tribeId, channel.sid)
  duser.addUserToChannel(db, channel, user, uid)
}
