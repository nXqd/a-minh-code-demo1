const AccessToken = require('twilio').jwt.AccessToken
const IpMessagingGrant = AccessToken.IpMessagingGrant
const R = require('ramda')
const Twilio = require('twilio').Twilio
const config = require('config')
const log = require('winston')
const shared = require('../shared')

/**
 * creates twilio client
 * @param accountSID
 * @param keySecret
 * @returns {*}
 */
exports.createTwilioClient = (accountSID, keySecret) => {
  return new Twilio(accountSID, keySecret)
}

/**
 * Inits firebase admin
 * @param {value} admin
 * @param {value} credsPath
 * @param {value} dbPath
 * @return {Null}
 */
exports.firebaseInit = (admin, credsPath, dbPath) => {
  log.info(admin.credential)
  admin.initializeApp({
    credential: admin.credential.cert(credsPath),
    databaseURL: dbPath
  })
}

/**
 * Gets firebase database from admin
 * @param admin
 */
exports.firebaseDatabase = (admin) => {
  return admin.database()
}

/**
 * Gets user's identity
 * @param user
 * @returns {String}
 */
exports.userIdentity = (user) => {
 return shared.userUserName(user)
}

/**
 * Creates chat service
 * @param twilioClient
 * @param chatServiceSID
 * @returns {Twilio.IpMessaging.V2.ServiceContext|Twilio.Chat.V2.ServiceContext|*}
 */
exports.createChatService = (twilioClient, chatServiceSID) => {
  return twilioClient.ipMessaging.v2.services(chatServiceSID)
}

exports.defaultChannelFriendlyName = "(Update Tribe Info)"

/**
 * Creates channel
 * @param {Object} twilioChatService
 * @param {string} uniqueName
 * @param {String} [friendlyName="(Update Tribe Info)"]
 * @returns {Promise.<*>}
 */
exports.createChannel = async (twilioChatService, uniqueName, friendlyName = exports.defaultChannelFriendlyName) => {
  try {
    return await
      twilioChatService
      .channels
      .create({
        friendlyName: friendlyName,
        uniqueName: uniqueName
      })
  } catch (err) {
    log.warn(`There is error while creating twilio channel named ${uniqueName}:  ${err}`)
    return null
  }
}

/**
 * Gets channel
 * @param {Object} twilioChatService
 * @param {string} uniqueName
 * @returns {Promise.<*>}
 */
exports.getChannel = async (twilioChatService, uniqueName) => {
  try {
    return await
      twilioChatService
        .channels
        .get(uniqueName)
  } catch (err) {
    log.error(`There is error while getting twilio channel named ${uniqueName}:  ${err}`)
    return null
  }
}

/**
 * Deletes channel
 * @param {Object} twilioChatService
 * @param {string} uniqueName
 * @returns {Promise.<*>}
 */
exports.removeChannel = async (twilioChatService, uniqueName) => {
  try {
    return await
      twilioChatService
        .channels
        .get(uniqueName)
        .remove()
  } catch (err) {
    log.error(`There is error while removing twilio channel with name ${uniqueName}: ${err}`)
    return null
  }
}

/**
 * Removes from channel
 * @param {object} twilioChatService
 * @param {String} channelSID
 * @param {String} memberSID
 * @returns {Promise.<*>}
 */
exports.removeUserFromChannel = async (twilioChatService, channelSID, memberSID) => {
  try {
    return await
      twilioChatService
        .channels(channelSID)
        .members(memberSID)
        .remove()
  } catch (err) {
    log.error(`Cannot remove user from channel ${err}`)
  }
}

/**
 * Removes from channel
 * @param {Object} channel
 * @param {String} userIdentity
 * @returns {Promise.<*>}
 */
exports.addUserToChannel = async (channel, userIdentity) => {
  try {
    return channel
      .members()
      .create({identity: userIdentity})
  } catch (err) {
    log.error(`error while adding user to channel: ${err}`)
    return null
  }
}

/**
 * Creates endpoint id
 * @param {String} identity
 * @param {String} deviceId
 * @returns {string}
 */
const createEndpointId = (identity, deviceId) => {
  return `MISSCHIEF:${identity}:${deviceId}`
}

/**
 * Generates access token
 * @param {String} identity
 * @param {String} deviceId
 * @returns {Promise.<*>}
 */
exports.generateAccessToken = async (identity, deviceId) => {
  if (R.isNil(identity)) {
    log.warn(`Identity is missing`)
    return
  }

  if (R.isNil(deviceId)) {
    log.warn(`DeviceId is missing`)
    return
  }

  const endpointId = createEndpointId(identity, deviceId)

  const sandboxIpmGrant = new IpMessagingGrant({
    serviceSid: config.get('twilio.chatServiceSID'),
    endpointId: endpointId,
    pushCredentialSid: config.get('twilio.sandboxAPNCredentialSid')
  })

  // const prodIpmGrant = new IpMessagingGrant({
  //   serviceSid: config.get('twilio.chatServiceSID'),
  //   endpointId: endpointId,
  //   pushCredentialSid: config.get('twilio.prodAPNCredentialSid')
  // })

  const token = new AccessToken(
    config.get('twilio.accountSID'),
    config.get('twilio.apiKey'),
    config.get('twilio.apiSecret'))

  token.addGrant(sandboxIpmGrant)
  // token.addGrant(prodIpmGrant)
  token.identity = identity

  return token.toJwt()
}


/**
 * Sugar function: Generates twilio access token for user
 * @param {Object} user
 * @param {String} device [APPLE|ANDROID]
 * @returns {Promise.<*>}
 */
exports.generateAccessTokenForUser = async (user, device) => {
  const deviceId = shared.userDeviceId(user, device)
  if (!deviceId) {
    log.error(`User's device id is missing`)
    return
  }

  const email = shared.userEmail(user)
  if (!email) {
    log.error(`User email is missing`)
    return
  }

  return await exports.generateAccessToken(email, deviceId)
}
